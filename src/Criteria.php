<?php

declare(strict_types=1);

namespace ZdenekGebauer\Db;

use DateTimeInterface;
use Dibi\Fluent;

/**
 * auxiliary object holding database criteria
 */
class Criteria
{
    /**
     * LIKE '%value%'
     */
    final public const LIKE_ANY = 'LIKE %%';

    /**
     * IN (values)
     */
    final public const IN = 'IN';

    /**
     * NOT IN (values)
     */
    final public const NOT_IN = 'NOT IN';

    /**
     * operator =
     */
    final public const EQUALS = '=';

    /**
     * operator <>
     */
    final public const NOT_EQUALS = '<>';

    /**
     * custom criteria
     */
    final public const CUSTOM = 'custom';

    /**
     * column IS NULL
     */
    final public const IS_NULL = 'IS NULL';

    /**
     * column IS NOT NULL
     */
    final public const IS_NOT_NULL = 'IS NOT NULL';

    /**
     * condition for column, ie:
     * new Criteria('id', Criteria::EQUALS, 123) // same as id=123
     * new Criteria('name', Criteria::LIKE_ANY, 'some') // `name` LIKE '%some%'
     * new Criteria('id', Criteria::IN, [2,3])
     * new Criteria('id', Criteria::NOT_IN, [2,3])
     * new Criteria('column', Criteria::IS_NULL)
     * new Criteria('column', Criteria::IS_NOT_NULL)
     * new Criteria('`column`>1 AND `column`<10 OR `column` IS NULL', Criteria::CUSTOM)
     *
     * @phpstan-param array<bool|int|float|string>|bool|DatetimeInterface|int|float|string|null $value
     */
    public function __construct(
        protected string $column,
        protected string $operator = self::EQUALS,
        protected array|bool|DateTimeInterface|float|int|string|null $value = null
    ) {
    }

    public function apply(Fluent $fluent): void
    {
        match ($this->operator) {
            self::LIKE_ANY => $fluent->where('[' . $this->column . '] LIKE %~like~', $this->value),
            self::IN, self::NOT_IN => $fluent->where(
                '[' . $this->column . '] ' . $this->operator . ' (?)',
                $this->value
            ),
            self::IS_NULL, self::IS_NOT_NULL => $fluent->where('[' . $this->column . '] ' . $this->operator),
            self::CUSTOM => $fluent->where($this->column),
            default => $fluent->where('[' . $this->column . ']' . $this->operator . '?', $this->value),
        };
    }
}

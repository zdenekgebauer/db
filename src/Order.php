<?php

declare(strict_types=1);

namespace ZdenekGebauer\Db;

use function in_array;

/**
 * ORDER BY value
 */
class Order
{
    final public const ASC = 'ASC';

    final public const DESC = 'DESC';

    final public const CUSTOM = 'custom';

    private readonly string $column;

    private readonly string $direction;

    /**
     * example
     * new Order('column', Order::DESC)
     * new Order('column ASC, other_column DESC', Order::CUSTOM)
     *
     */
    public function __construct(string $column, string $direction = self::ASC)
    {
        $this->column = trim($column);
        $this->direction = (in_array($direction, [self::DESC, self::CUSTOM], true) ? $direction : self::ASC);
    }

    /**
     * returns part of sql for ordering
     */
    public function getOrderBy(): string
    {
        if ($this->direction === self::CUSTOM) {
            return $this->column;
        }
        return '[' . $this->column . '] ' . $this->direction;
    }
}

<?php

declare(strict_types=1);

namespace ZdenekGebauer\Db;

use function count;

/**
 * @template T of Entity
 */
abstract class Repository
{
    /**
     * @param Mapper<T> $mapper
     */
    public function __construct(protected readonly Mapper $mapper)
    {
    }

    public function count(Filter $filter): int
    {
        return $this->mapper->count($this->getCriteria($filter));
    }

    /**
     * @return array<Criteria>
     */
    protected function getCriteria(Filter $filter): array
    {
        $criteria = [];
        if ($filter->filterId > 0) {
            $criteria[] = new Criteria($this->mapper->getPrimaryKey(), Criteria::EQUALS, $filter->filterId);
        }
        if ($filter->getFilterIds() !== []) {
            $criteria[] = new Criteria($this->mapper->getPrimaryKey(), Criteria::IN, $filter->getFilterIds());
        }
        return $criteria;
    }

    /**
     * @param T $entity
     */
    public function delete(Entity $entity): void
    {
        $this->mapper->deleteById($entity->id);
        $entity->setDeleted();
    }

    /**
     * @return Collection<T>
     */
    public function findAll(int $max = 1_000): Collection
    {
        $filter = new Filter();
        $filter->paginator->setItemsPerPage($max);
        return $this->find($filter);
    }

    /**
     * @return T|null
     */
    public function findById(int $id): ?Entity
    {
        if ($id <= 0) {
            return null;
        }
        $filter = new Filter();
        $filter->filterId = $id;
        $filter->paginator->setItemsPerPage(1);
        return $this->find($filter)->getFirst();
    }

    /**
     * @return Collection<T>
     */
    public function find(Filter $filter): Collection
    {
        return new Collection(
            $this->mapper->find(
                $this->getCriteria($filter),
                $this->getOrder($filter),
                $filter->getOffset(),
                $filter->getLimit()
            )
        );
    }

    /**
     * @return array<Order>
     */
    protected function getOrder(Filter $filter): array
    {
        return [];
    }

    /**
     * @param int[] $ids
     * @return Collection<T>
     */
    public function findByIds(array $ids): Collection
    {
        if ($ids === []) {
            return new Collection([]);
        }
        $filter = new Filter();
        $filter->setFilterIds($ids);
        $filter->paginator->setItemsPerPage(count($ids));
        return $this->find($filter);
    }

    /**
     * @param T $entity
     */
    public function save(Entity $entity): void
    {
        $this->mapper->save($entity);
    }
}

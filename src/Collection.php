<?php

declare(strict_types=1);

namespace ZdenekGebauer\Db;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Traversable;

use function count;

/**
 * @template T of Entity
 * @implements IteratorAggregate<T>
 */
class Collection implements IteratorAggregate, Countable
{
    /**
     * @var array<T>
     */
    protected array $entities = [];

    /**
     * @param array<T> $entities
     */
    public function __construct(array $entities)
    {
        array_walk($entities, static function (Entity $entity) {
        });
        $this->entities = $entities;
    }

    /**
     * @param T $entity
     */
    public function add(Entity $entity): void
    {
        $this->entities[] = $entity;
    }

    public function count(): int
    {
        return count($this->entities);
    }

    /**
     * @return array<int, T>
     */
    public function entities(): array
    {
        return $this->entities;
    }

    /**
     * @return T of Entity|null
     */
    public function getFirst(): ?Entity
    {
        return $this->entities === [] ? null : reset($this->entities);
    }

    /**
     * @return Traversable<int, T>
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->entities);
    }

    public function isEmpty(): bool
    {
        return $this->entities === [];
    }

    /**
     * @return array<int>
     */
    public function getIds(): array
    {
        return array_values(array_map(static fn(Entity $entity) => $entity->id, $this->entities));
    }
}

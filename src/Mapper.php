<?php

declare(strict_types=1);

namespace ZdenekGebauer\Db;

use DateTimeInterface;
use Dibi\Fluent;
use Dibi\IConnection;
use Dibi\Row;
use LogicException;

/**
 * @template T of Entity
 */
abstract class Mapper
{
    /**
     * @var array<Order>
     */
    protected array $order = [];

    public function __construct(protected readonly IConnection $database)
    {
    }

    /**
     * @param array<Criteria> $criteria
     */
    public function count(array $criteria = []): int
    {
        $fluent = $this->database->select('COUNT(*)')
            ->from($this->getTableName());
        $this->applyCriteria($fluent, $criteria);
        return $fluent->fetchSingle();
    }

    abstract public function getTableName(): string;

    /**
     * @param array<Criteria> $criteria
     */
    protected function applyCriteria(Fluent $fluent, array $criteria): void
    {
        foreach ($criteria as $criterion) {
            $criterion->apply($fluent);
        }
    }

    public function deleteById(int $entityId): void
    {
        $fluent = $this->database->delete($this->getTableName());
        $this->applyCriteria($fluent, [new Criteria($this->getPrimaryKey(), Criteria::EQUALS, $entityId)]);
        $fluent->execute();
    }

    /**
     * name of column with primary key
     */
    public function getPrimaryKey(): string
    {
        return 'id';
    }

    /**
     * @param array<Criteria> $criteria
     * @return T|null
     */
    public function findOne(array $criteria): ?Entity
    {
        $entities = $this->find($criteria, [], 0, 1);
        return $entities === [] ? null : reset($entities);
    }

    /**
     * @param array<Criteria> $criteria
     * @param array<Order> $order
     * @param int|null $offset first returned rows
     * @param int|null $limit (max count of returned rows, null=all)
     * @return array<int, T>
     */
    public function find(array $criteria = [], array $order = [], ?int $offset = null, ?int $limit = null): array
    {
        if ($this->getColumnsSelect() === []) {
            throw new LogicException(
                'empty columns for select data, fix method getColumnsSelect() in class ' . static::class
            );
        }
        $this->order = $order;
        $entities = [];
        $fluent = $this->prepareSqlSelect();
        if ($offset > 0) {
            $fluent->offset($offset);
        }
        if ($limit > 0) {
            $fluent->limit($limit);
        }
        $this->applyOrderBy($fluent, $order);
        $this->applyCriteria($fluent, $criteria);
        foreach ($fluent->fetchAssoc($this->getPrimaryKey()) as $row) {
            $entities[(int)$row[$this->getPrimaryKey()]] = $this->createEntity($row);
        }
        return $entities;
    }

    /**
     * @return array<string>
     */
    abstract protected function getColumnsSelect(): array;

    protected function prepareSqlSelect(?int $offset = null, ?int $limit = null): Fluent
    {
        return $this->database->select($this->getColumnsSelect())
            ->from($this->getTableName())
            ->offset($offset)
            ->limit($limit);
    }

    /**
     * @param array<Order> $order
     */
    protected function applyOrderBy(Fluent $fluent, array $order): void
    {
        foreach ($order as $value) {
            $fluent->orderBy($value->getOrderBy());
        }
    }

    /**
     * @return T
     */
    abstract protected function createEntity(Row $row): Entity;

    /**
     * @param T $entity
     */
    public function save(Entity $entity): void
    {
        if ($entity->isNew()) {
            $insertColumns = $this->getColumnsInsert($entity);
            if ($insertColumns !== []) {
                $this->database->insert($this->getTableName(), $insertColumns)->execute();
                $entity->setId($this->database->getInsertId());
            }
            return;
        }
        $this->update($entity);
    }

    /**
     * data for insert new entity in format [column] => value
     *
     * @param T $entity
     * @return array<string, DateTimeInterface|float|int|string|null>
     */
    abstract protected function getColumnsInsert(Entity $entity): array;

    /**
     * @param T $entity
     */
    public function update(Entity $entity): void
    {
        $this->database
            ->update($this->getTableName(), $this->getColumnsUpdate($entity))
            ->where('[' . $this->getPrimaryKey() . '] = ?', $entity->id)
            ->execute();
    }

    /**
     * data for update entity in format [column]=>value
     *
     * @param T $entity
     * @return array<string, DateTimeInterface|float|int|string|null>
     */
    protected function getColumnsUpdate(Entity $entity): array
    {
        return $this->getColumnsInsert($entity);
    }
}

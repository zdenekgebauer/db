<?php

declare(strict_types=1);

namespace ZdenekGebauer\Db;

use BadMethodCallException;
use Nette\Utils\Paginator;

use function array_key_exists;
use function is_array;

class Filter
{
    public string $order = '';

    public readonly Paginator $paginator;

    public int $filterId = 0;

    /**
     * @var array<string, array<int|string>|bool|int|string>
     */
    private array $filters = [];

    /**
     * @var array<int>
     */
    private array $filterIds = [];

    public function __construct()
    {
        $this->paginator = new Paginator();
    }

    public function getFilter(string $name): bool|float|int|string
    {
        if (!array_key_exists($name, $this->filters)) {
            return '';
        }
        if (is_array($this->filters[$name])) {
            throw new BadMethodCallException("for filter '$name' use method getFilterArray()");
        }
        return $this->filters[$name];
    }

    public function getFilterInt(string $name): int
    {
        return (int) $this->getFilter($name);
    }

    /**
     * @return array<bool|int|string>
     */
    public function getFilterArray(string $name): array
    {
        if (!array_key_exists($name, $this->filters)) {
            return [];
        }
        return is_array($this->filters[$name]) ? $this->filters[$name] : [$this->filters[$name]];
    }

    /**
     * @return array<int>
     */
    public function getFilterIds(): array
    {
        return $this->filterIds;
    }

    /**
     * @param array<int> $ids
     */
    public function setFilterIds(array $ids): void
    {
        $this->filterIds = array_filter(array_map('\intval', $ids));
        $this->setLimit(100_000);
    }

    /**
     * @return array<string, array<int|string>|bool|int|string>
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    public function getLimit(): int
    {
        return $this->paginator->getLength();
    }

    public function getOffset(): int
    {
        return $this->paginator->getOffset();
    }

    public function setLimit(int $limit): void
    {
        $this->paginator->setItemsPerPage($limit);
    }

    /**
     * @param array<int, int|string>|bool|int|string $value
     */
    public function setFilter(string $name, array|bool|int|string $value): void
    {
        $this->filters[$name] = $value;
    }

    public function setTotal(int $total): void
    {
        $this->paginator->setItemCount($total);
    }
}

<?php

declare(strict_types=1);

namespace ZdenekGebauer\Db;

abstract class Entity
{
    protected bool $deleted = false;

    public function __construct(public int $id = 0)
    {
    }

    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    public function setDeleted(): void
    {
        $this->deleted = true;
    }

    public function isNew(): bool
    {
        return $this->id <= 0;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }
}

<?php

declare(strict_types=1);

namespace ZdenekGebauer\Db;

use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Dibi\UniqueConstraintViolationException;
use Exception;

/**
 * methods for preparing sql commands
 */
class Helper
{
    public static function boolFromSql(float|int|string $value): bool
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public static function boolToSql(bool $value): int
    {
        return $value ? 1 : 0;
    }

    public static function isDuplicateKey(UniqueConstraintViolationException $exception, string $key): bool
    {
        return str_contains($exception->getMessage(), "for key '" . $key . "'");
    }

    public static function utcDate(bool|DateTimeInterface|float|int|string|null $value): ?DateTimeImmutable
    {
        if ($value instanceof DateTimeInterface) {
            $value = $value->format('Y-m-d H:i:s');
        }
        if (empty($value)) {
            return null;
        }

        try {
            return new DateTimeImmutable((string)$value, new DateTimeZone('UTC'));
        } catch (Exception) {
            return null;
        }
    }
}

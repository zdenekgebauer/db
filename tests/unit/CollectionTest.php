<?php

declare(strict_types=1);

namespace Unit;

use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\Db\Collection;
use ZdenekGebauer\Db\Entity;

class CollectionTest extends Unit
{
    protected UnitTester $tester;

    public function testAdd(): void
    {
        $collection = new Collection([]);
        $this->tester->assertTrue($collection->isEmpty());

        $newId = 10;
        $collection->add(
            new class($newId) extends Entity {
            }
        );
        $this->tester->assertFalse($collection->isEmpty());
        $this->tester->assertCount(1, $collection);
        $this->tester->assertEquals([$newId], $collection->getIds());
    }
}

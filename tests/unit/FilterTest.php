<?php

declare(strict_types=1);

namespace Tests\Unit;

use BadMethodCallException;
use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\Db\Filter;

class FilterTest extends Unit
{
    protected UnitTester $tester;

    public function testPagination(): void
    {
        $filter = new Filter();
        $filter->setTotal(100);
        $filter->setLimit(10);

        $this->tester->assertEquals(10, $filter->paginator->getLastPage());
    }

    public function testGetFilters(): void
    {
        $filter = new Filter();
        $filter->setFilter('string', 'B');
        $filter->setFilter('int', 2);
        $filter->setFilter('array', ['q', 'w', 'e']);

        $this->tester->assertEquals(['string' => 'B', 'int' => 2, 'array' => ['q', 'w', 'e']], $filter->getFilters());
        $this->tester->assertEquals(['B'], $filter->getFilterArray('string'));

        $this->tester->expectThrowable(
            new BadMethodCallException("for filter 'array' use method getFilterArray()"),
            static function () use ($filter) {
                $filter->getFilter('array');
            }
        );
    }
}

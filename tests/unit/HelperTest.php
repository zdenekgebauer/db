<?php

declare(strict_types=1);

namespace Tests\Unit;

use DateTimeImmutable;
use DateTimeZone;
use Dibi\UniqueConstraintViolationException;
use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\Db\Helper;

class HelperTest extends Unit
{
    protected UnitTester $tester;

    public function testisDuplicateKey(): void
    {
        $this->tester->assertFalse(
            Helper::isDuplicateKey(new UniqueConstraintViolationException("duplicate value for key 'index'"), 'name')
        );
        $this->tester->assertTrue(
            Helper::isDuplicateKey(
                new UniqueConstraintViolationException("duplicate value for key 'index'"),
                'index'
            )
        );
    }

    public function testUtcDate(): void
    {
        $this->tester->assertNull(Helper::utcDate(null));

        $dateString = '2020-07-01 10:20:30';
        $utcDate = new DateTimeImmutable($dateString, new DateTimeZone('UTC'));

        $localDate = new DateTimeImmutable($dateString, new DateTimeZone('Europe/Prague'));
        $this->tester->assertEquals($utcDate, Helper::utcDate($localDate));
        $this->tester->assertEquals($utcDate, Helper::utcDate($localDate->format('Y-m-d H:i:s')));

        $localDate = new DateTimeImmutable($dateString, new DateTimeZone('UTC'));
        $this->tester->assertEquals($utcDate, Helper::utcDate($localDate));
        $this->tester->assertEquals($utcDate, Helper::utcDate($localDate->format('Y-m-d H:i:s')));

        $localDate = new DateTimeImmutable($dateString, new DateTimeZone('America/New_York'));
        $this->tester->assertEquals($utcDate, Helper::utcDate($localDate));
        $this->tester->assertEquals($utcDate, Helper::utcDate($localDate->format('Y-m-d H:i:s')));

        $this->tester->assertNull(Helper::utcDate(''));
        $this->tester->assertNull(Helper::utcDate('invalid'));
        $this->tester->assertNull(Helper::utcDate(0));
        $this->tester->assertNull(Helper::utcDate(10));
        $this->tester->assertNull(Helper::utcDate(true));
    }
}

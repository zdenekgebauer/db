<?php

declare(strict_types=1);

use Codeception\Util\Autoload;

require_once __DIR__ . '/TestInstance.php'; // due gitlab

Autoload::addNamespace('', 'src');


<?php

declare(strict_types=1);

namespace Tests\Integration;

use Codeception\Test\Unit;
use DateTime;
use Dibi\UniqueConstraintViolationException;
use Tests\Support\IntegrationTester;
use ZdenekGebauer\Db\Filter;
use ZdenekGebauer\Db\Helper;

class RepositoryTest extends Unit
{

    protected IntegrationTester $tester;

    public function testCount(): void
    {
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            [
                'id' => 1,
                'string_var' => 'A',
                'int_var' => -1,
                'float_var' => -12.34,
                'date_var' => '2020-01-01',
                'bool_var' => 1,
            ]
        );
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            ['id' => 2, 'string_var' => 'B', 'int_var' => 0, 'float_var' => 0, 'date_var' => null, 'bool_var' => 1]
        );

        $repository = $this->tester->createRepository();

        $filter = new Filter();
        $this->tester->assertEquals(2, $repository->count($filter));

        $filter->filterId = 2;
        $this->tester->assertEquals(1, $repository->count($filter));
        $filter->filterId = 9999;
        $this->tester->assertEquals(0, $repository->count($filter));
    }

    public function testDelete(): void
    {
        $idToDelete = 1;
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            [
                'id' => $idToDelete,
                'string_var' => 'A',
                'int_var' => 0,
                'float_var' => 0,
                'date_var' => null,
                'bool_var' => 1,
            ]
        );
        $idToRemain = 10;
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            [
                'id' => $idToRemain,
                'string_var' => 'B',
                'int_var' => 0,
                'float_var' => 0,
                'date_var' => null,
                'bool_var' => 1,
            ]
        );

        $entity = new TestInstance();
        $entity->id = $idToDelete;
        $this->tester->assertFalse($entity->isDeleted());

        $repository = $this->tester->createRepository();
        $repository->delete($entity);

        $this->tester->assertTrue($entity->isDeleted());
        $this->tester->dontSeeInDatabase(MapperTest::TABLE_NAME, ['id' => $idToDelete]);
        $this->tester->seeInDatabase(MapperTest::TABLE_NAME, ['id' => $idToRemain]);
    }

    public function testFind(): void
    {
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            [
                'id' => 1,
                'string_var' => 'A',
                'int_var' => -1,
                'float_var' => -12.34,
                'date_var' => '2020-01-01',
                'bool_var' => 1,
            ]
        );
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            ['id' => 2, 'string_var' => 'B', 'int_var' => 0, 'float_var' => 0, 'date_var' => null, 'bool_var' => 1]
        );
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            ['id' => 3, 'string_var' => 'BB', 'int_var' => 1, 'float_var' => 0, 'date_var' => null, 'bool_var' => 0]
        );

        $repository = $this->tester->createRepository();

        $filter = new Filter();
        $collection = $repository->find($filter);
        $this->tester->assertFalse($collection->isEmpty());
        $this->tester->assertCount(1, $collection);
        foreach ($collection as $item) {
            $this->tester->assertInstanceOf(TestInstance::class, $item);
        }
        $this->tester->assertInstanceOf(TestInstance::class, $collection->getFirst());

        // get all rows
        $collection = $repository->findAll(999);
        $this->tester->assertCount(3, $collection);

        // filter by string
        $filter = new Filter();
        $filter->setFilter('string', 'B');
        $collection = $repository->find($filter);
        $this->tester->assertCount(1, $collection);
        $this->tester->assertEquals(2, $collection->getFirst()->id);

        // filter by int
        $filter = new Filter();
        $filter->setFilter('integer', '-1');
        $collection = $repository->find($filter);
        $this->tester->assertCount(1, $collection);
        $this->tester->assertEquals(1, $collection->getFirst()->id);

        // filter by array
        $filter = new Filter();
        $filter->setFilter('array', ['-1', '1']);
        $filter->setLimit(100_000);
        $collection = $repository->find($filter);
        $this->tester->assertCount(2, $collection);
        $this->tester->assertEquals([1, 3], $collection->getIds());

        // ORDER BY RAND()
        $filter = new Filter();
        $filter->setLimit(999);
        $filter->order = 'RAND()';
        $collection = $repository->find($filter);
        $ids = $collection->getIds();
        $this->tester->assertCount(3, $ids);
        $this->tester->assertContains(1, $ids);
        $this->tester->assertContains(2, $ids);
        $this->tester->assertContains(3, $ids);
    }

    public function testFindById(): void
    {
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            [
                'id' => 1,
                'string_var' => 'string var',
                'int_var' => -1,
                'float_var' => -12.34,
                'date_var' => '2020-01-01',
                'bool_var' => 1,
            ]
        );

        $repository = $this->tester->createRepository();
        $entity = $repository->findById(1);

        $this->tester->assertEquals(1, $entity->id);
        $this->tester->assertEquals('string var', $entity->stringProperty);
    }

    public function testFindByIdWithInvalidId(): void
    {
        $repository = $this->tester->createRepository();
        $this->tester->assertNull($repository->findById(-1));
    }

    public function testFindByIds(): void
    {
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            [
                'id' => 1,
                'string_var' => 'string var1',
                'int_var' => -1,
                'float_var' => -12.34,
                'date_var' => '2020-01-01',
                'bool_var' => 1,
            ]
        );
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            [
                'id' => 2,
                'string_var' => 'string var2',
                'int_var' => -1,
                'float_var' => -12.34,
                'date_var' => '2020-01-01',
                'bool_var' => 1,
            ]
        );
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            [
                'id' => 3,
                'string_var' => 'string var3',
                'int_var' => -1,
                'float_var' => -12.34,
                'date_var' => '2020-01-01',
                'bool_var' => 1,
            ]
        );

        $repository = $this->tester->createRepository();
        $entities = $repository->findByIds([2, 4, 3]);
        $keys = array_keys($entities->entities());
        $this->tester->assertCount(2, $keys);
        $this->tester->assertContains(2, $keys);
        $this->tester->assertContains(3, $keys);

        // empty array of ids
        $entities = $repository->findByIds([]);
        $this->tester->assertEquals([], $entities->entities());
    }

    public function testInsert(): void
    {
        $entity = new TestInstance();

        $stringVariable = 'some string';
        $intVariable = 20;
        $floatVariable = 123.45;
        $dateVariable = new DateTime();
        $boolVariable = true;

        $entity->stringProperty = $stringVariable;
        $entity->integerProperty = $intVariable;
        $entity->floatProperty = $floatVariable;
        $entity->dateProperty = $dateVariable;
        $entity->boolProperty = $boolVariable;

        $this->tester->assertTrue($entity->isNew());

        $repository = $this->tester->createRepository();
        $repository->save($entity);

        $this->tester->assertFalse($entity->isNew());
        $this->tester->assertEquals(1, $entity->id);
        $this->tester->assertEquals($stringVariable, $entity->stringProperty);
        $this->tester->assertEquals($intVariable, $entity->integerProperty);
        $this->tester->assertEquals($floatVariable, $entity->floatProperty);
        $this->tester->assertEquals(
            $dateVariable->format('Y-m-d H:i:s'),
            $entity->dateProperty->format('Y-m-d H:i:s')
        );
        $this->tester->assertTrue($entity->boolProperty);

        $row = $this->tester->grabEntryFromDatabase(MapperTest::TABLE_NAME, ['id' => 1]);
        $this->tester->assertEquals($stringVariable, $row['string_var']);
        $this->tester->assertEquals($intVariable, $row['int_var']);
        $this->tester->assertEquals($floatVariable, $row['float_var']);
        $this->tester->assertEquals($dateVariable->format('Y-m-d H:i:'), substr((string)$row['date_var'], 0, 17));
        $this->tester->assertTrue(Helper::boolFromSql($row['bool_var']));

        // duplicate insert
        $entity2 = clone $entity;
        $entity2->id = 0;

        $this->tester->expectThrowable(
            new UniqueConstraintViolationException("Duplicate entry 'some string' for key 'idx_string'", 1062),
            static function () use ($repository, $entity2) {
                $repository->save($entity2);
            }
        );
    }

    public function testUpdate(): void
    {
        $id = 10;
        $this->tester->haveInDatabase(
            MapperTest::TABLE_NAME,
            [
                'id' => $id,
                'string_var' => 'old_str',
                'int_var' => 0,
                'float_var' => 0,
                'date_var' => null,
                'bool_var' => 1,
            ]
        );

        $stringVariable = 'new string';
        $intVariable = -20;
        $floatVariable = -123.45;
        $dateVariable = new DateTime();
        $boolVariable = false;

        $entity = new TestInstance();
        $entity->id = $id;
        $entity->stringProperty = $stringVariable;
        $entity->integerProperty = $intVariable;
        $entity->floatProperty = $floatVariable;
        $entity->dateProperty = $dateVariable;
        $entity->boolProperty = $boolVariable;

        $this->tester->assertFalse($entity->isNew());

        $repository = $this->tester->createRepository();
        $repository->save($entity);

        $this->tester->assertFalse($entity->isNew());
        $this->tester->assertEquals($id, $entity->id);
        $this->tester->assertEquals($stringVariable, $entity->stringProperty);
        $this->tester->assertEquals($intVariable, $entity->integerProperty);
        $this->tester->assertEquals($floatVariable, $entity->floatProperty);
        $this->tester->assertEquals(
            $dateVariable->format('Y-m-d H:i:s'),
            $entity->dateProperty->format('Y-m-d H:i:s')
        );
        $this->tester->assertFalse($entity->boolProperty);

        $row = $this->tester->grabEntryFromDatabase(MapperTest::TABLE_NAME, ['id' => $id]);
        $this->tester->assertEquals($stringVariable, $row['string_var']);
        $this->tester->assertEquals($intVariable, $row['int_var']);
        $this->tester->assertEquals($floatVariable, $row['float_var']);
        $this->tester->assertEquals($dateVariable->format('Y-m-d H:i:'), substr((string)$row['date_var'], 0, 17));
        $this->tester->assertFalse(Helper::boolFromSql($row['bool_var']));
    }

    protected function _after(): void
    {
        $this->tester->truncate([MapperTest::TABLE_NAME]);
    }

    protected function _before(): void
    {
        $this->tester->truncate([MapperTest::TABLE_NAME]);
    }
}

<?php

declare(strict_types=1);

namespace Tests\Integration;

use DateTimeInterface;
use ZdenekGebauer\Db\Entity;

class TestInstance extends Entity
{

    public string $stringProperty = '';

    public int $integerProperty = 0;

    public float $floatProperty = 0;

    public ?DateTimeInterface $dateProperty = null;

    public bool $boolProperty = false;

}

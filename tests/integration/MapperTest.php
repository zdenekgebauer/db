<?php

declare(strict_types=1);

namespace Tests\Integration;

use Codeception\Test\Unit;
use LogicException;
use Tests\Support\IntegrationTester;
use ZdenekGebauer\Db\Criteria;
use ZdenekGebauer\Db\Order;

class MapperTest extends Unit
{

    final public const TABLE_NAME = 'test_table';

    protected IntegrationTester $tester;

    public function testFind(): void
    {
        $mapper = $this->tester->createMapper();

        $this->tester->executeQuery(
            "INSERT INTO `test_table` (`id`, `string_var`, `int_var`, `float_var`, `date_var`, `bool_var`) VALUES
                (1, 'A', -1, -12.34, '2020-01-01', 1),
                (2, 'B', 1, -12.34, NULL, 1),
                (3, 'BB', 3, -12.34, NULL, 1);"
        );

        $entities = $mapper->find();
        $this->tester->assertCount(3, $entities);

        $entities = $mapper->find([], [new Order('id')], 2);
        $this->tester->assertCount(1, $entities);
        $this->tester->assertEquals([3], array_keys($entities));

        // like
        $entities = $mapper->find(
            [new Criteria('string_var', Criteria::LIKE_ANY, 'B')],
            [new Order('id', Order::DESC)]
        );
        $this->tester->assertCount(2, $entities);
        $this->tester->assertEquals([3, 2], array_keys($entities));

        // null
        $entities = $mapper->find(
            [new Criteria('date_var', Criteria::IS_NULL)],
            [new Order('id', Order::DESC)]
        );
        $this->tester->assertCount(2, $entities);
        $this->tester->assertEquals([3, 2], array_keys($entities));

        $entities = $mapper->find(
            [new Criteria('date_var', Criteria::IS_NOT_NULL)],
            [new Order('id', Order::ASC)]
        );
        $this->tester->assertCount(1, $entities);
        $this->tester->assertEquals([1], array_keys($entities));

        // IN
        $entities = $mapper->find(
            [new Criteria('int_var', Criteria::IN, [-1, 1])],
            [new Order('id', Order::DESC)]
        );
        $this->tester->assertEquals([2, 1], array_keys($entities));

        // custom criteria and order
        $entities = $mapper->find(
            [new Criteria("date_var = '0000-00-00' OR date_var IS NULL", Criteria::CUSTOM)],
            [new Order('id DESC, date_var ASC', Order::CUSTOM)]
        );
        $this->tester->assertEquals([3, 2], array_keys($entities));

        // findOne
        $entity = $mapper->findOne(
            [new Criteria('string_var', Criteria::LIKE_ANY, 'B')],
        );
        $this->tester->assertInstanceOf(TestInstance::class, $entity);
    }

    public function testFindWithUndefinedColumns(): void
    {
        $mapper = $this->tester->createMapperWithUndefinedColumns();
        $this->tester->expectThrowable(
            LogicException::class,
            static function () use ($mapper) {
                $mapper->find();
            }
        );
    }
}

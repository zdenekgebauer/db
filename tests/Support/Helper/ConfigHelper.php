<?php

declare(strict_types=1);

namespace Tests\Support\Helper;

use Codeception\Configuration;
use Codeception\Module;
use Codeception\TestInterface;
use LogicException;

class ConfigHelper extends Module
{
    private string $currentEnv = '';

    public function _before(TestInterface $test)
    {
        $currentEnv = $test->getMetadata()->getCurrent('env');
        if ($currentEnv === null) {
            throw new LogicException('missing codeception command line parameter "environment"');
        }
        $this->currentEnv = $currentEnv;
    }

    /**
     * @return array{host:string, database:string, user:string, password:string, charset:string}
     */
    public function getDbConfiguration(): array
    {
        $config = Configuration::config();
        if (!isset($config['env'][$this->currentEnv]['modules']['config']['Db'])) {
            throw new LogicException(
                "missing configuration 'modules > config > Db' in environment '$this->currentEnv'"
            );
        }
        $dbConfig = $config['env'][$this->currentEnv]['modules']['config']['Db'];

        $dsnVars = [];
        $dsnWithoutDriver = preg_replace('/^[a-z]+:/i', '', (string)$dbConfig['dsn']);
        foreach (explode(';', $dsnWithoutDriver) as $item) {
            $keyValueTuple = explode('=', $item);
            [$key, $value] = array_values($keyValueTuple);
            $dsnVars[$key] = $value;
        }

        return [
            'host' => $dsnVars['host'],
            'database' => $dsnVars['dbname'],
            'user' => $dbConfig['user'],
            'password' => $dbConfig['password'],
            'charset' => $dsnVars['charset'] ?? 'utf8mb4',
        ];
    }

    public function getEnvironmentParameter(string $name): string
    {
        $config = Configuration::config();
        if (isset($config['env'][$this->currentEnv][$name])) {
            return $config['env'][$this->currentEnv][$name];
        }
        throw new LogicException("missing parameter '$name' in environment '$this->currentEnv'");
    }
}

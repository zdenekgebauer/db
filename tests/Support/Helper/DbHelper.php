<?php

declare(strict_types=1);

namespace Tests\Support\Helper;

use Codeception\Exception\ModuleException;
use Codeception\Lib\Driver\Db;
use Codeception\Lib\Driver\MySql;
use Codeception\Module;
use Codeception\TestInterface;
use Exception;

use function is_array;

class DbHelper extends Module
{
    private Db $driver;

    public function _before(TestInterface $test)
    {
        $this->driver = $this->getModule('Db')->_getDriver();
    }

    /**
     * truncate table
     *
     * @param array<string>|string $table
     */
    public function truncate(array|string $table): void
    {
        $tables = is_array($table) ? $table : [$table];
        $isMysql = $this->driver instanceof MySql;

        if ($isMysql) {
            $this->executeQuery('SET FOREIGN_KEY_CHECKS=0');
        }
        foreach ($tables as $tableToEmpty) {
            $this->executeQuery('TRUNCATE TABLE ' . $this->driver->getQuotedName($tableToEmpty));
        }
        if ($isMysql) {
            $this->executeQuery('SET FOREIGN_KEY_CHECKS=1');
        }
    }

    public function executeQuery(string $sql): void
    {
        $this->debugSection('Query', $sql);
        $this->driver->executeQuery($sql, []);
    }
}

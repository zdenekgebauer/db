-- see MapperTest::TABLE_NAME
DROP TABLE IF EXISTS `test_table`;

CREATE TABLE `test_table`
(
    `id`         INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
    `string_var` VARCHAR(50)         NOT NULL,
    `int_var`    INT(10)             NOT NULL,
    `float_var`  DECIMAL(10, 2)      NOT NULL,
    `date_var`   DATETIME            NULL     DEFAULT NULL,
    `bool_var`   TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE INDEX `idx_string` (`string_var`)
)
    COLLATE = 'utf8mb4_general_ci'
    ENGINE = InnoDB;

-- see MapperNoPkTest::TABLE_NAME
DROP TABLE IF EXISTS `test_table_no_pk`;

CREATE TABLE `test_table_no_pk`
(
    `string_var` VARCHAR(50)         NOT NULL,
    `int_var`    INT(10)             NOT NULL,
    `float_var`  DECIMAL(10, 2)      NOT NULL,
    `date_var`   DATETIME            NULL     DEFAULT NULL,
    `bool_var`   TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
    INDEX `idx_string` (`string_var`)
)
    COLLATE = 'utf8mb4_general_ci'
    ENGINE = InnoDB;

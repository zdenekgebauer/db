<?php

declare(strict_types=1);

namespace Tests\Support;

use Dibi\Row;
use Dibi\Connection;
use Tests\Integration\MapperTest;
use Tests\Integration\TestInstance;
use ZdenekGebauer\Db\Criteria;
use ZdenekGebauer\Db\Filter;
use ZdenekGebauer\Db\Helper;
use ZdenekGebauer\Db\Entity;
use ZdenekGebauer\Db\Mapper;
use ZdenekGebauer\Db\Repository;

/**
 * Inherited Methods
 * @method void wantTo($text)
 * @method void wantToTest($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause($vars = [])
 *
 * @SuppressWarnings(PHPMD)
 */
class IntegrationTester extends \Codeception\Actor
{
    use _generated\IntegrationTesterActions;

    /**
     * Define custom actions here
     */

    public function createMapper(): Mapper
    {
        return new class ($this->getDibiConnection()) extends Mapper {

            public function getTableName(): string
            {
                return MapperTest::TABLE_NAME;
            }

            protected function getColumnsSelect(): array
            {
                return ['id', 'string_var', 'int_var', 'float_var', 'date_var', 'bool_var'];
            }

            protected function getColumnsInsert(Entity $entity): array
            {
                return [
                    'string_var' => $entity->stringProperty,
                    'int_var' => $entity->integerProperty,
                    'float_var' => $entity->floatProperty,
                    'date_var' => $entity->dateProperty,
                    'bool_var' => Helper::boolToSql($entity->boolProperty),
                ];
            }

            protected function createEntity(Row $row): Entity
            {
                $entity = new TestInstance();
                $entity->id = $row['id'];
                $entity->stringProperty = $row['string_var'];
                $entity->integerProperty = $row['int_var'];
                $entity->floatProperty = $row['float_var'];
                $entity->dateProperty = $row['date_var'];
                $entity->boolProperty = Helper::boolFromSql($row['bool_var']);
                return $entity;
            }
        };
    }

    public function createMapperWithUndefinedColumns(): Mapper
    {
        return new class ($this->getDibiConnection()) extends Mapper {

            public function getTableName(): string
            {
                return MapperTest::TABLE_NAME;
            }

            protected function getColumnsSelect(): array
            {
                return [];
            }

            protected function getColumnsInsert(Entity $entity): array
            {
                return [];
            }

            protected function createEntity(Row $row): Entity
            {
                return new TestInstance();
            }
        };
    }

    public function createRepository(): Repository
    {
        return new class ($this->createMapper()) extends Repository {
            /**
             * @return array<Criteria>
             */
            protected function getCriteria(Filter $filter): array
            {
                $criteria = parent::getCriteria($filter);
                if ($filter->getFilter('string') !== '') {
                    $criteria[] = new Criteria('string_var', Criteria::EQUALS, $filter->getFilter('string'));
                }
                if ($filter->getFilterInt('integer') !== 0) {
                    $criteria[] = new Criteria('int_var', Criteria::EQUALS, $filter->getFilterInt('integer'));
                }
                if ($filter->getFilterArray('array') !== []) {
                    $criteria[] = new Criteria('int_var', Criteria::IN, $filter->getFilterArray('array'));
                }
                return $criteria;
            }
        };
    }

    private function getDibiConnection(): Connection
    {
        $dbConfig = $this->getDbConfiguration();

        return new Connection(
            [
                'driver' => 'mysqli',
                'host' => $dbConfig['host'],
                'username' => $dbConfig['user'],
                'password' => $dbConfig['password'],
                'database' => $dbConfig['database'],
            ]
        );
    }
}
